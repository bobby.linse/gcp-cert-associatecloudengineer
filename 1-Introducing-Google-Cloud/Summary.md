# Introducing Google Cloud

## How did we get here

The first wave of the trend towards cloud computing was colocation. Colocation gave users the financial efficiency of renting physical space, instead of investing in data center real estate.

Virtualized data centers of today, the second wave, share similarities with the private data centers and colocation facilities of decades past. The components of virtualized data centers match the physical building blocks of hosted computing—servers, CPUs, disks, load balancers, and so on—but now they are virtual devices. Virtualization does provide a number of benefits: your development teams can move faster, and you can turn capital expenses into operating expenses. With virtualization you still maintain the infrastructure; it is still a user-controlled/user-configured environment.

About 10 years ago, Google realized that its business couldn’t move fast enough within the confines of the virtualization model. So Google switched to a container-based architecture—a fully automated, elastic third-wave cloud that consists of a combination of automated services and scalable data. Services automatically provision and configure the infrastructure used to run applications.

Today, Google Cloud Platform makes this third-wave cloud available to Google customers.

![History](../assets/1_GCP-Services_History.png)

## GCP Services: Why Customers Choose

### Digital Architecture

Virtualized data centers brought you infrastructure as a service (IaaS) and platform as a service (PaaS) offerings. IaaS offerings provide you with raw compute, storage, and network, organized in ways familiar to you from physical and virtualized data centers. PaaS offerings, on the other hand, bind your code to libraries that provide access to the infrastructure your application needs, thus allow you to focus on your application logic.

In the IaaS model, you pay for what you allocate. In the PaaS model, you pay for what you use. 

As cloud computing has evolved, the momentum has shifted toward managed infrastructure and managed services.

![Architecture](../assets/1_GCP-Services-Why-Choose_Architecture.png)

### Multi-Regional and Zones

#### Regions and zones

Regions are independent geographic areas that consist of zones. Locations within regions tend to have round-trip network latencies of under 5 milliseconds on the 95th percentile.

A zone is a deployment area for Google Cloud Platform resources within a region. Think of a zone as a single failure domain within a region. In order to deploy fault-tolerant applications with high availability, you should deploy your applications across multiple zones in a region to help protect against unexpected failures.

To protect against the loss of an entire region due to natural disaster, you should have a disaster recovery plan and know how to bring up your application in the unlikely event that your primary region is lost.

For more information on the specific resources available within each location option, see Google’s Global Data Center Locations[^1]. Google Cloud Platform's services and resources can be zonal, regional, or managed by Google across multiple regions. For more information on what these options mean for your data, see geographic management of data[^2].

#### Zonal resources

Zonal resources operate within a single zone. If a zone becomes unavailable, all of the
zonal resources in that zone are unavailable until service is restored:

- Google Compute Engine VM instance resides within a specific zone.

#### Regional resources

Regional resources are deployed with redundancy within a region. This gives them
higher availability relative to zonal resources.

#### Multi-regional resources

A few Google Cloud Platform services are managed by Google to be redundant and distributed within and across regions. These services optimize availability, performance, and resource efficiency. As a result, these services require a trade-off on either latency or the consistency model. These trade-offs are documented on a product-specific basis.

The following services have one or more multi-regional
deployments in addition to any regional deployments:
    - Google App Engine and its features
    - Google Cloud Datastore
    - Google Cloud Storage
    - Google BigQuery

### Environmental Responsibility

![Environment](../assets/1_GCP-Services-Why-Choose_Environment.png)

This image shows Google’s data center in Hamina, Finland. The facility is one of the most advanced and efficient data centers in the Google fleet. Its cooling system, which uses sea water from the Bay of Finland, reduces energy use and is the first of its kind anywhere in the world. Google is one of the world’s largest corporate purchasers of wind and solar energy. Google has been 100% carbon neutral since 2007, and will shortly reach 100% renewable energy sources for its data centers. The virtual world is built on physical infrastructure, and all those racks of humming servers use vast amounts of energy. Together, all existing data centers use roughly 2% of the world’s electricity. So Google works to make data centers run as efficiently as possible. Google’s data centers were the first to achieve ISO 14001 certification, a standard that maps out a framework for improving resource efficiency and reducing waste.

### Customer-Friendly Pricing

![Pricing](../assets/1_GCP-Services-Why-Choose_Pricing.png)

Google was the first major cloud provider to deliver per-second billing for its Infrastructure-as-a-Service compute offering, Google Compute Engine. Per-second billing is offered for users of Compute Engine, Kubernetes Engine (container infrastructure as a service), Cloud Dataproc (the open-source Big Data system Hadoop as a service), and App Engine flexible environment VMs (a Platform as a Service). Google Compute Engine offers automatically applied sustained-use discounts, which are automatic discounts that you get for running a virtual-machine instance for a significant portion of the billing month. Specifically, when you run an instance for more than 25% of a month, Compute Engine automatically gives you a discount for every incremental minute you use for that instance. Custom virtual machine types allow Google Compute Engine virtual machines to be fine-tuned for their applications, so that you can tailor your pricing for your workloads.

Try the online pricing calculator to help estimate your costs[^3]

### Open Source Services

![Services](../assets/1_GCP-Services-Why-Choose_Services.png)

![Storage](../assets/1_GCP-Services-Why-Choose_Storage.png)

Google gives customers the ability to run their applications elsewhere if Google becomes no longer the best provider for their needs.

This includes:
    - Using Open APIs. Google services are compatible with open-source products. For example, Google Cloud Bigtable, a horizontally scalable managed database: Bigtable uses the Apache HBase interface, which gives customersthe benefit of code portability. Another example: Google Cloud Dataproc offers the open-source big data environment Hadoop as a managed service.
    - Google publishes key elements of its technology, using open-source licenses,to create ecosystems that provide customers with options other than Google. For example, TensorFlow, an open-source software library for machine learning developed inside Google, is at the heart of a strong open-source ecosystem.
    - Google provides interoperability at multiple layers of the stack. Kubernetes and Google Kubernetes Engine give customers the ability to mix and match microservices running across different clouds. Google Stackdriver lets customers monitor workloads across multiple cloud providers.

### Security

| Layer                   | Notable security measures (among others)                                                                         |
| ----------------------- | -----------------------------------------------------------------------------------------------------------------|
| Operational security    | Intrusion detection systems; techniques to reduce insider risk; employee U2F use; software development practices |
| Internet communication  | Google Front End; designed-in Denial of Service protection                                                       |
| Storage services        | Encryption at rest                                                                                               |
| User identity           | Central identity service with support for U2F                                                                    |
| Service deployment      | Encryption of inter-service communication                                                                        |
| Hardware infrastructure | Hardware design and provenance; secure boot stack; premises security                                             |

#### Hardware design and provenance

Both the server boards and the networking equipment in Google data centers are custom-designed by Google. Google also designs custom chips, including a hardware security chip that is currently being deployed on both servers and peripherals.

#### Secure boot stack

Google server machines use a variety of technologies to ensure that they are booting the correct software stack, such as cryptographic signatures over the BIOS, bootloader, kernel, and base operating system image.

#### Premises security

Google designs and builds its own data centers, which incorporate multiple layers of physical security protections. Access to these data centers is limited to only a very small fraction of Google employees. Google additionally hosts some servers in third-party data centers, where we ensure that there are Google-controlled physical security measures on top of the security layers provided by the data center operator.

#### Encryption of inter-service communication

Google’s infrastructure provides cryptographic privacy and integrity for remote procedure call (“RPC”) data on the network. Google’s services communicate with each other using RPC calls. The infrastructure automatically encrypts all infrastructure RPC traffic which goes between data centers. Google has started to deploy hardware cryptographic accelerators that will allow it to extend this default encryption to all infrastructure RPC traffic inside Google data centers.

#### User identity

Google’s central identity service, which usually manifests to end users
as the Google login page, goes beyond asking for a simple username and password.
The service also intelligently challenges users for additional information based on risk
factors such as whether they have logged in from the same device or a similar
location in the past. Users also have the option of employing second factors when
signing in, including devices based on the Universal 2nd Factor (U2F) open standard

#### Encryption at rest

Most applications at Google access physical storage indirectly via
storage services, and encryption (using centrally managed keys) is applied at the
layer of these storage services. Google also enables hardware encryption support in
hard drives and SSDs.

#### Google Front End (“GFE”)

Google services that want to make themselves available
on the Internet register themselves with an infrastructure service called the Google
Front End, which ensures that all TLS connections are terminated using correct
certificates and following best practices such as supporting perfect forward secrecy.
The GFE additionally applies protections against Denial of Service attacks.
Denial of Service (“DoS”) protection: The sheer scale of its infrastructure enables
Google to simply absorb many DoS attacks. Google also has multi-tier, multi-layer
DoS protections that further reduce the risk of any DoS impact on a service running
behind a GFE.

#### Intrusion detection

Rules and machine intelligence give operational security
engineers warnings of possible incidents. Google conducts Red Team exercises to
measure and improve the effectiveness of its detection and response mechanisms.

#### Reducing insider risk

Google aggressively limits and actively monitors the activities of
employees who have been granted administrative access to the infrastructure.

#### Employee U2F use

To guard against phishing attacks against Google employees,
employee accounts require use of U2F-compatible Security Keys.

#### Software development practices

Google employs central source control and requires
two-party review of new code. Google also provides its developers libraries that
prevent them from introducing certain classes of security bugs. Google also runs a
Vulnerability Rewards Program where we pay anyone who is able to discover and
inform us of bugs in our infrastructure or applications.

### Info-Graphics

What is cloud computing?

![Cloud-Computing](../assets/1_GCP-What-Is-Cloud-Computing.png)

## Resources

### Quizzes

#### Quiz - GCP Regions and Zones

1. Why might a GCP customer use resources in several zones within a region?
    - For improved fault tolerance
        - Reasoning: As part of building a fault-tolerant application, you can spread your resources across multiple zones in a region.
1. Why might a GCP customer use resources in several regions around the world?
    - To bring their applications closer to users around the world, and for improved fault tolerance

#### Quiz - Introducing Google Cloud Platform

1. Choose a fundamental characteristic of devices in a virtualized data center.
    - They are manageable separately from the underlying hardware.
1. Choose fundamental characteristics of cloud computing. Mark all that are correct (4 correct responses).
    - Customers pay only for what they use or reserve
    - Customers can scale their resource use up and down
    - Resources are available from anywhere over the network
    - Computing resources available on-demand and self-service
1. What kind of customer benefits most from billing by the second for cloud resources such as virtual machines?
    - Customers who create and run many virtual machines
1. What type of cloud computing service lets you bind your application code to libraries that give access to the infrastructure your application needs?
    - Platform as a Service
1. Which statement is true about the zones within a region?
    - The zones within a region have fast network connectivity among them.
1. What type of cloud computing service provides raw compute, storage, and network, organized in ways that are familiar from physical data centers?
    - Infrastructure as a Service

### Videos

- Introduction to Google Cloud Platform - https://youtu.be/EHpaHQaUZgE
- What is cloud computing - https://youtu.be/TpW2zkqQt0A
- How did we get here? - https://youtu.be/Ntxkz-ltIqo
- Open APIs - https://youtu.be/0Gpbyqj2ptM

### Provided Resources

- Why Google Cloud Platform? - https://cloud.google.com/why-google/
- Pricing philosophy - https://cloud.google.com/pricing/philosophy/
- Data centers - https://www.google.com/about/datacenters/
- Google Cloud Platform product overview - http://cloud.google.com/products/
- Google Cloud Platform solutions - http://cloud.google.com/solutions/

### Referenced Links

[^1]: GCP - Data-Centers: https://cloud.google.com/about/datacenters/

[^2]: GCP - Geographic Management of Data: https://cloud.google.com/docs/geography-and-regions#geographic_management_of_data

[^3]: GCP - Online Pricing Calculator: https://cloud.google.com/products/calculator/